from rs485 import RS485Listener


def get_listener(conf_path: str = None):
    return RS485Listener(conf_path)
