import os
import yaml

listener_port = os.environ.get('RS485_PORT', 'COM3')

conf_path = os.environ.get('RS485_CONF_PATH', 'data/config.yaml')

with open(conf_path) as serial_listener_conf_file:
    def_config = yaml.load(serial_listener_conf_file, Loader=yaml.FullLoader)

