import serial
import yaml
import logging
from settings import def_config


ser = serial.Serial(
    port='COM3',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    xonxoff=False,
    rtscts=False,
    dsrdtr=True,
    timeout=1
)

logger = logging.getLogger(__name__)

commands = [['Sense', '0', '0', ['ScanAuto', 'ScanManual'], ['CameraOn', 'CameraOff'], 'IrisClose', 'IrisOpen', 'FocusNear'],
            ['FocusFar', 'ZoomWide', 'ZoomTele', 'Down', 'Up', 'Left', 'Right', '0']]


def read_parse_data():
    data = ""
    if ser.inWaiting() > 0:
        packet = ser.read(7)
        address = packet[1]
        command1 = packet[2]
        command2 = packet[3]
        pan = packet[4]
        tilt = packet[5]

        # parse command
        command1 = format(command1, '#010b')[2:]
        command2 = format(command2, '#010b')[2:]

        text_command1 = ''
        text_command2 = ''

        for i in range(8):
            if command1[i] == str(1) and i > 2:
                if i == 3 or i == 4:
                    text_command1 += commands[0][i][int(command1[0])] + ' '
                else:
                    text_command1 += commands[0][i] + ' '

            if command2[i] == str(1):
                text_command2 += commands[1][i] + ' '

        if 'Down' in text_command2 or 'Up' in text_command2 or 'Left' in text_command2 or 'Right' in text_command2:
            data = {'Joystick': True, 'name': text_command2, 'pan': pan, 'tilt': tilt}
        else:
            data = (text_command1 + text_command2).strip()
    return data


def receiving():
    while True:
        read_parse_data()


def main():
    receiving()


class RS485Listener:
    def __init_(self, conf_path: str = None):
        if conf_path is None:
            self.conf = def_config
        else:
            with open(conf_path) as conf_file:
                self.conf = yaml.load(conf_file, Loader=yaml.FullLoader)
        self.move_msg = self.conf['move_msg']

    def listen(self):
        data = read_parse_data()
        if 'Joystick' in data:  # Нужно отловить, что данные пришли с джойстика
            if 'Up' in data['name'] or 'Down' in data['name']:
                moveYMsg = self.conf['MoveY']
                moveYMsg['data']['value'] = lambda y: y if 'Up' in data['name'] else -y
                return moveYMsg
            if 'Left' in data['name'] or 'Right' in data['name']:
                moveXMsg = self.conf['MoveX']
                moveXMsg['data']['value'] = lambda x: x if 'Right' in data['name'] else -x
                return moveXMsg
        if data in self.conf:
            return self.conf[data]

    def echo(self):
        while True:
            raw = read_parse_data()
            if 'Joystick' in raw:  # Нужно отловить, что данные пришли с джойстика
                if 'Up' in raw['name'] or 'Down' in raw['name']:
                    moveYMsg = self.conf['MoveY']
                    moveYMsg['data']['value'] = lambda y: y if 'Up' in raw['name'] else -y
                    return moveYMsg
                if 'Left' in raw['name'] or 'Right' in raw['name']:
                    moveXMsg = self.conf['MoveX']
                    moveXMsg['data']['value'] = lambda x: x if 'Right' in raw['name'] else -x
                    return moveXMsg
            if raw in self.conf:
                return self.conf[raw]
